Rails.application.routes.draw do
	
  	devise_for :users, :controllers => {:omniauth =>
  	 "devise/callbacks", :registrations => "devise/registrations", :sessions => "devise/sessions"}
		devise_scope :user do
	  		namespace :api do
	   		namespace :v1 do
	   			resources :registrations
	      		resources :sessions
	      		resources :posts do
	      			collection do
          				post :apply
        			end
        		end
	      		resources :job_applications
	    	end
	    end
  	end
end