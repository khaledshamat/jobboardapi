class Post < ApplicationRecord

	has_many :job_applications
	validates :title, presence:true
	validates :body, presence:true
	
end
