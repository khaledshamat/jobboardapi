class User < ApplicationRecord
	
	has_many :job_applications
	acts_as_token_authenticatable

	validates :email, presence:true
	validates :password, presence:true

  	devise	:database_authenticatable, :registerable,
        	:recoverable, :rememberable, :validatable
end
