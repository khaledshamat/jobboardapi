class Api::V1::PostsController <  ApplicationController
	
	#Fix Me 
	#Needs to be specified for admin and user
	before_action :authenticate_user!, only:[:apply, :show, :create, :destroy, :update]
	
	def index
		posts = Post.order('created_at DESC')
		render json: {status: 'Success', message: 'Loaded Posts', data:posts}, status: :ok
	end

	def show
		post = Post.find(params[:id])
		render json: {status: 'Success', message: 'Loaded Post', data:post}, status: :ok
	end

	def create
		post = Post.new(post_params)
		if post.save
			render json: {status: 'Success', message: 'Post Saved', data:post}, status: :ok
		else
			render json: {status: 'Failed', message: 'Post Not Saved', data:post.errors}, status: :unprocessable_entity
		end
	end

	def destroy
		post = Post.destroy(params[:id])
		render json: {status: 'Success', message: 'Post Deleted', data:post}, status: :ok
	end

	def update
		post = Post.find(params[:id])
		if post.update_attributes(post_params)
			render json: {status: 'Success', message: 'Post Updated', data:post}, status: :ok
		else
			render json: {status: 'Failed', message: 'Post Not Updated', data:post.errors}, status: :unprocessable_entity
		end
	end

	def apply
		post = Post.find(params[:id])
		user = User.find_by_authentication_token(:authentication_token)
		application = JobApplication.create('',0, user, post)
		if application.save
			render json: {status: 'Success', message: 'Post Saved', data:application}, status: :ok
		else
			render json: {status: 'Failed', message: 'Post Saved', data:application}, status: :ok
		end
	end

	private

	def post_params
		params.permit(:title, :body)
	end

	def jobApplication_params
		params.permit(:data, :status, :user)
	end
end
