module Api
	module V1
		class JobApplicationsController < ApplicationController
			
			before_action :authenticate_user!

			def index
				app = JobApplication.order('created_at DESC')
				render json: {status: 'Success', message: 'Loaded Applications', data:app}, status: :ok
			end

			def show
				app = JobApplication.find(params[:id])
				render json: {status: 'Success', message: 'Loaded app', data:app}, status: :ok
			end

			def create

				#I was trying to find a way to make the user apply to a job post by finding the 
				#user that created the job application then find the post to add its id to make
				#it belongs to an application then create the job application. 

				#Not working
				#data = params[:data]
				#status = params[:status]
				#user_id = params[:user_id]
				#post_id = params[:post_id]
				#user = User.find(id: user_id)
				#app = JobApplication.new(data: data, status: status, user: user, post_id: post_id)

				app = JobApplication.new(app_params)
				if app.save
					render json: {status: 'Success', message: 'Application Saved', data:app}, status: :ok
				else
					render json: {status: 'failed', message: 'Application Not Saved', data:app}, status: :unprocessable_entity
				end
			end

			def destroy
				app = JobApplication.destroy(params[:id])
				render json: {status: 'Success', message: 'Application Deleted', data:app}, status: :ok
			end

			def update
				app = JobApplication.find(params[:id])
				if app.update_attributes(app_params)
					render json: {status: 'Success', message: 'Application Updated', data:app}, status: :ok
				else
					render json: {status: 'Failed', message: 'Application Not Updated', data:post.errors}, status: :unprocessable_entity
				end
			end

			private
			def app_params
				params.permit(:data, :status, :user_id)
			end

		end
	end
end
	