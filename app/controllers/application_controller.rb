class ApplicationController < ActionController::API
  	
  	include ActionController::MimeResponds
 	include ActionController::RequestForgeryProtection
  	protect_from_forgery with: :exception, prepend: true	
	skip_before_action :verify_authenticity_token
  

	protected

	def json_request?
	    request.format.json?
	end
end