class AddPostToJobApplications < ActiveRecord::Migration[5.2]
  def change
    add_column :job_applications, :post_id, :posts
  end
end
